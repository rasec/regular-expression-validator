$(function(){
	$("#regexp, #regexpvalue").on("change keyup paste focus", function(evt){
		var regexpVal = $("#regexp").val();
		$("#regexpvalue").attr("pattern", regexpVal);
		$("#regexpvalue").attr("title", regexpVal);
		$("#regexpvalue").attr("placeholder", regexpVal);
		// Check Validity
		$("#regexpvalue")[0].checkValidity();
	});
});